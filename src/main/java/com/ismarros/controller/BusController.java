package com.ismarros.controller;

import com.ismarros.model.Bus;
import com.ismarros.model.BusRoute;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/bus")
public class BusController {

//    Variáveis finais para consumir a API
//    http://datapoa.com.br/group/about/mobilidade , neste site
//    realizar a integração com as operações de linhas de ônibus e itinerário.

    //    - Listar as linhas de ônibus -
    final String listBusLines = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";

    //    - Listar itinerário de uma determinada unidade de transporte -
    final String listBusByIdLine = "http://www.poatransporte.com.br/php/facades/process.php?a=il&p=5566";


    //Possível solução para conversão de text/html para json, porém não consigo entender direito
//    @Bean
//    public RestTemplate restTemplate() {
//        RestTemplate template = new RestTemplate(clientHttpRequestFactory());
//        List<HttpMessageConverter<?>> converters = template.getMessageConverters();
//        for (HttpMessageConverter<?> converter : converters) {
//            if (converter instanceof MappingJackson2HttpMessageConverter) {
//                try {
//                    converter.getSupportedMediaTypes().add(MediaType.TEXT_HTML);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return template;
//    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Collection<Bus>> getAllBusLines() {
        RestTemplate restTemplate = new RestTemplate();
        List<Bus> busList = new ArrayList<>();

        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("www.poatransporte.com.br")
                .path("php/facades/process.php")
                .queryParam("a", "nc")
                .queryParam("p", "%")
                .queryParam("t", "o")
                .build();

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        //Add the Jackson Message converter
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        // Note: here we are making this converter to process any kind of response,
        // not only application/*json, which is the default behaviour
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);

        busList.addAll(restTemplate.getForObject(uri.toUriString(), Collection.class));

        return new ResponseEntity<Collection<Bus>>(busList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<BusRoute> getBusById(@PathVariable("id") Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        BusRoute busRoute = new BusRoute();

        //http://www.poatransporte.com.br/php/facades/process.php?a=il&p=5566

        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("www.poatransporte.com.br")
                .path("php/facades/process.php?a=il&")
                .queryParam("p", id)
                .build();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        //Add the Jackson Message converter
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        // Note: here we are making this converter to process any kind of response,
        // not only application/*json, which is the default behaviour
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);

        System.out.println(restTemplate.getForObject(uri.toUriString(), BusRoute.class).getIdLinha());
        busRoute = restTemplate.getForObject(uri.toUriString(), BusRoute.class);


        return new ResponseEntity<BusRoute>(busRoute, HttpStatus.OK);
    }

}
