package com.ismarros.model;

import java.io.Serializable;
import java.util.List;

public class BusRoute implements Serializable {

    private String idLinha;

    private String nome;

    private String codigo;

    private List<String> listaRotas;

    public String getIdLinha() {
        return idLinha;
    }

    public void setIdLinha(String idLinha) {
        this.idLinha = idLinha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<String> getListaRotas() {
        return listaRotas;
    }

    public void setListaRotas(List<String> listaRotas) {
        this.listaRotas = listaRotas;
    }
}
